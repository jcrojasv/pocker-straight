// Devuelve true/false si las cartas tienen una escalera
const isStraight = function (cards) {
  let qtCards = cards.length;
  cards.sort((a, b) => a - b);
  // validar que el array no contengan numeros menor a 2 y mayor a 14
  if ((cards[0] >= 2 && cards[qtCards -1] <= 14) && (qtCards >= 5 && qtCards <= 7)) {

    if (cards[0] == 2 && cards[qtCards -1] == 14 ) {
      qtCards = cards.unshift(1);
    }
    let cont = 1;
    let result = 0;
    for (let i = 0; i < qtCards - 1; i++ ) {
      result = (cards[i] - cards[i+1]);
      cont = (result  == -1 || result == 0) ? cont+1 : 1;
      if (cont >= 5) {
        return true;
      }
    }
    return false;

  } else {
    return false;
  }
};

module.exports = isStraight;