const isStraight = require('../straight');

test("isStraight([2, 3, 4, 5, 6])", () => {
  const result = isStraight([2, 3, 4 ,5, 6]);
  expect(result).toBeTruthy();
});

test("isStraight([14, 5, 4 ,2, 3]) debe retornar true el 14 es un AS y 14", () => {
  const result = isStraight([14, 5, 4, 2, 3]);
  expect(result).toBeTruthy();
});

test("isStraight([2, 3]) debe retornar falso", () => {
  const result = isStraight([2, 3]);
  expect(result).toBeFalsy();
});

test("isStraight([7, 7, 12 ,11, 3, 4, 14]) debe retornar falso", () => {
  const result = isStraight([7, 7, 12, 11, 3, 4, 14]);
  expect(result).toBeFalsy();
});

test("isStraight([7, 7, 12, 11, 3, 4, 14]) debe retornar falso", () => {
  const result = isStraight([7, 7, 12, 11, 3, 4, 14]);
  expect(result).toBeFalsy();
});

test("isStraight([2, 7, 12, 11, 10, 13, 14]) debe retornar verdadero", () => {
  const result = isStraight([2, 7, 12, 11, 10, 13, 14]);
  expect(result).toBeTruthy();
});

test("isStraight([9, 10, 11, 12, 13]) debe retornar verdadero", () => {
  const result = isStraight([9, 10, 11, 12, 13]);
  expect(result).toBeTruthy();
});

